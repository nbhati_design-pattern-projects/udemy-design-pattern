package com.structural.flyweight;

public enum BrushSize {
    THIN, MEDIUM, THICK
}