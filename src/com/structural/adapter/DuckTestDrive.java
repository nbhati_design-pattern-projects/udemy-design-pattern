package com.structural.adapter;

public class DuckTestDrive {
	public static void main(String[] args) {
		
		
		System.out.println("The Turkey says...");
		Turkey turkey = new WildTurkey();
		turkey.gobble();
		turkey.fly();
		
		
	
		System.out.println("\nThe Duck says...");
		Duck duck = new MallardDuck();
		testDuck(duck);
		
		//Now, let�s say you�re short on Duck objects and you�d like to use some 
		//Turkey objects in their place. Obviously we can�t use the turkeys outright because they have a different interface.
		
		
		//Duck is target interface
		//Turkey is adaptee
		//User wants turkey but he is exposed only to Duck(Target Inteface) 
		
		Duck turkeyAdapter = new TurkeyAdapter(turkey);
		System.out.println("\nThe TurkeyAdapter says...");
		testDuck(turkeyAdapter);
	}

	static void testDuck(Duck duck) {
		duck.quack();
		duck.fly();
	}
}