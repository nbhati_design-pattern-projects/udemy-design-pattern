package com.structural.flyweight2;

interface Player
{
    public void assignWeapon(String weapon);
    public void mission();
}
