package com.behavioural.command;

public class RemoteControlTest {
	public static void main(String[] args) {
		SimpleRemoteControl remote = new SimpleRemoteControl();

		Light light = new Light();
		
		LightOnCommand lightOn = new LightOnCommand(light);
		remote.setCommand(lightOn);
		remote.buttonWasPressed();
		
		LightOffCommand lightOff = new LightOffCommand(light);
		remote.setCommand(lightOff);
		remote.buttonWasPressed();
		
		
		GarageDoor garageDoor = new GarageDoor();
		
		GarageDoorOpenCommand garageOpen = new GarageDoorOpenCommand(garageDoor);
		remote.setCommand(garageOpen);
		remote.buttonWasPressed();

	}
	
	
	// Concrete Command - LightOnCommand,GarageDoorOpenCommand
	// Receiver - Light,GarageDoor
	// Invoker - SimpleRemoteControl
}
