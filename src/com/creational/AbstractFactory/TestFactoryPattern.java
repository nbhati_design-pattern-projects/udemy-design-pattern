package com.creational.AbstractFactory;

public class TestFactoryPattern
{
  public static void main(String[] args)
  {
    System.out.println(CarFactory.buildCar(CarType.SMALL, Location.ASIA));
    System.out.println(CarFactory.buildCar(CarType.SEDAN, Location.DEFAULT));
    System.out.println(CarFactory.buildCar(CarType.LUXURY, Location.USA));
  }
}