package com.creational.AbstractFactory;

public enum Location {
	  DEFAULT, USA, ASIA
	}