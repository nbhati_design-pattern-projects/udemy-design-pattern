package com.creational.factory;

public interface Notification {
    void notifyUser();
}