package com.solid;

public class OCP {

	public static void main(String[] args) {

		Cube c1 = new Cube();
		c1.length = 2;

		Cube c2 = new Cube();
		c2.length = 2;

		Cube c3 = new Cube();
		c3.length = 2;

		Cube[] cArray = new Cube[3];
		cArray[0] = c1;
		cArray[1] = c2;
		cArray[2] = c3;

		Sphere s = new Sphere();
		s.radius = 1;

		Sphere[] sArray = new Sphere[1];
		sArray[0] = s;

		OCP ocp = new OCP();
		System.out.println(ocp.calculateArea(cArray, sArray));

		/////////////////////////////////////////////////////

		Objects[] objArray = new Objects[4];

		objArray[0] = c1;
		objArray[1] = c2;
		objArray[2] = c3;
		objArray[3] = s;

		System.out.println(ocp.calculateArea2(objArray));
	}

	private double calculateArea(Cube[] c, Sphere[] s) {

		double ans = 0;
		for (Sphere sphere : s) {
			ans = ans + ((3.14) * sphere.radius *sphere.radius);
		}

		for (Cube cube : c) {
			ans = ans + (cube.length * cube.length);
		}

		return ans;
	}

	private double calculateArea2(Objects[] objArray) {

		double ans = 0;
		for (Objects obj : objArray) {
			ans = ans + obj.getArea();
		}

		return ans;
	}

}

abstract class Objects {

	abstract double getArea();

}

class Cube extends Objects {

	int length;

	@Override
	double getArea() {
		return length * length;
	}

}

class Sphere extends Objects {

	int radius;

	@Override
	double getArea() {
		return 3.14 * radius * radius;
	}

}
